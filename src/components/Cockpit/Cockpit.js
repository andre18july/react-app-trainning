import React , { useEffect, useRef, useContext } from 'react';
import styles from './Cockpit.module.css';
import AuthContext from '../../context/auth-context';


const cockpit = (props) => {

    const toggleBtnRef =useRef(null);
    const authContext = useContext(AuthContext);

    console.log(authContext.authenticated);
    
    useEffect(() => {
        console.log('[Cockpit.js] useEffect');
        
        setTimeout(() => {
            toggleBtnRef.current.click();
        }, 1000);
            
        

        return () => {

            console.log('[Cockpit.js] cleanup work in useEffect');
        }

    }, []);

    useEffect(() => {
        console.log('[Cockpit.js] 2nd useEffect');
        return () => {
            console.log('[Cockpit.js] cleanup work in 2nd useEffect');
        }
    })

    const classes = [];
    let btnClass = '';

    if(props.showPersons) {
        btnClass = styles.Red;
    }

    if(props.personsLength <= 2){
    classes.push(styles.red);
    }

    if(props.personsLength <= 1){
    classes.push(styles.bold);
    }

    return(
        <div className={styles.Cockpit}>
            <h1>{props.title}</h1>
            <p className={classes.join(" ")}>This is really working...</p>
        
            <button 
                onClick={() => props.switchNameHandler('Andre')}>Switch Name
            </button>

            <button 
                ref={toggleBtnRef}
                className={btnClass} 
                id="togPersons" onClick={props.togglePersonsHandler}>Toggle Persons
            </button>


            <button onClick={authContext.login}>Log in</button>

        
        </div>
    );
};

export default React.memo(cockpit);