import React , { Component } from 'react';
import styles from './Person.module.css';
//import Auxilary from '../../../hoc/Auxilary';
import withClass from '../../../hoc/withClass';
import PropTypes from 'prop-types';
import AuthContext from '../../../context/auth-context';

class Person extends Component {

    constructor(props){
        super(props);
        this.inputElementRef = React.createRef();
    }

    static contextType = AuthContext;

    componentDidMount(){
        this.inputElementRef.current.focus();
        console.log(this.context.authenticated);
    }

    render(){
        console.log('[Person.js] Rendering...');
        return(

            <>

                {this.context.authenticated ? <p>User authenticated</p> : <p>Please log in</p>}


                <p onClick={this.props.click}>I'm a Person and my name is {this.props.name} and I am {this.props.age} years old!</p>
                <p>{this.props.children}</p>
                <input 
                    type="text" 
                    //ref={(inputEl) => {this.inputElement = inputEl}}
                    ref={this.inputElementRef}
                    onChange={this.props.changed} 
                    value={this.props.name}
                />

            </>
        );
    }

}

Person.propTypes = {
    click:  PropTypes.func,
    name: PropTypes.string,
    age: PropTypes.number,
    change: PropTypes.func
};


export default withClass(Person, styles.Person);