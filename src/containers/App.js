import React, { Component } from 'react';
import styles from './App.module.css';
import Persons from '../components/Persons/Persons';
import Cockpit from '../components/Cockpit/Cockpit';
import withClass from '../hoc/withClass';
import Auxilary from '../hoc/Auxilary';
import AuthContext  from '../context/auth-context';


class App extends Component{

  constructor(props){
    super(props);
    console.log('[App.js] Constructor');
  }

  state = {
    persons: [
      { id: 'p1', name: 'Max', age: 29 },
      { id: 'p2', name: 'Manu', age: 30 },
      { id: 'p3', name: 'John', age: 51 }
    ],
    showPersons: false,
    showCockpit: true, 
    countChanges: 0,
    authenticated: false
  };

  static getDerivedStateFromProps(props, state){
    console.log('[App.js] getDerivedStateFromProps', props);

    return state;
  }

  componentDidMount(){
    console.log('[App.js] componentDidMount');
  }

  shouldComponentUpdate(nextProps, nextState){
    console.log('[App.js] shouldComponentUpdate');
    return true;
  }

  componentDidUpdate(){
    console.log('[App.js] componentDidUpdate');
  }

  switchNameHandler = (newName) => {
    this.setState({
      persons: [
        { id: 'p1', name: newName, age: 29 },
        { id: 'p2', name: 'Manu', age: 30 },
        { id: 'p3', name: 'John', age: 51 }
      ]
    })
  };

  nameChangedHandler = (event, id) => {

    const personIndex = this.state.persons.findIndex(p => p.id === id);

    const person = {
      ...this.state.persons[personIndex]
    };

    person.name = event.target.value;

    const persons = [...this.state.persons];
    persons[personIndex] = person;

    this.setState((prevState, props) => {
      return {
        persons: persons,
        countChanges: prevState.countChanges + 1
      }
    })
  };


  togglePersonsHandler = () => {
    const doesShow = this.state.showPersons;
    this.setState({
      showPersons: !doesShow
    });
  }


  deletePersonHandler = (personIndex) => {
    //const persons = this.state.persons.slice();
    const persons = [...this.state.persons];
    persons.splice(personIndex, 1);
    this.setState({persons: persons});
  }

  loginHandler = () => {
    this.setState({
      authenticated: true
    }); 
  }

  render(){
    console.log('[App.js] Render');
    let persons = null;


    if(this.state.showPersons){
      
      persons = (
        
        <div>
          <Persons 
            persons={this.state.persons}
            clicked={this.deletePersonHandler}
            changed={this.nameChangedHandler}
            isAuthenticated={this.state.authenticated}
          />
        </div>

      )
    }


    return (

      <Auxilary>

        <button onClick={() => this.setState({showCockpit: false})}>Remove cockpit</button>

        <AuthContext.Provider 
          value={{
            authenticated: this.state.authenticated,
            login: this.loginHandler
          }}
        
        >
          {
            this.state.showCockpit ? 
              (<Cockpit
                title={this.props.appTitle} 
                personsLength={this.state.persons.length} 
                showPersons={this.state.showPersons} 
                switchNameHandler={this.switchNameHandler}
                togglePersonsHandler={this.togglePersonsHandler}
              />)
            : null
          }
          {persons}
        </AuthContext.Provider>

      
      </Auxilary>

    );
  }
}

export default withClass(App, styles.App);
